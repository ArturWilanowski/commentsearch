package com.arturwilanowski.demoPost.dto;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Getter;
import lombok.Setter;

@JsonFilter("commentFilter")
public class CommentDTO {

    @Getter
    @Setter
    String postId;
    @Getter
    @Setter
    String id;
    @Getter
    @Setter
    String name;
    @Getter
    @Setter
    String email;
    @Getter
    @Setter
    String body;

    public CommentDTO(String postId, String id, String name, String email, String body) {
        this.postId = postId;
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    @Override
    public String toString() {
        return "Comment for postId= " + postId +
                " commentId=" + id +
                " name=" + name +
                " email=" + email +
                ", commentBody='" + body + '\'' +
                '.';
    }
}
