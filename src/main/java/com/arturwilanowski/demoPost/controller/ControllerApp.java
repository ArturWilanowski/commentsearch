package com.arturwilanowski.demoPost.controller;

import com.arturwilanowski.demoPost.repository.CommentsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ControllerApp {

    @Autowired
    CommentsRepo commentsRepo;

    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String index(@RequestParam(required = false) String postId,
                        @RequestParam(required = false) String commentId,
                        Model model) {
        String idPost;
        String idComment;

        if (postId != null && !postId.isEmpty()) {
            idPost = postId;
            model.addAttribute("idPost", idPost);
            model.addAttribute("commentsList", commentsRepo.getResponseByPostId(idPost));
        } else if (commentId != null && !commentId.isEmpty()) {
            idComment = commentId;
            model.addAttribute("idComment", idComment);
            model.addAttribute("commentsList", commentsRepo.getResponseById(idComment));
        }
        model.addAttribute("view", "views/commentsForm");
        return "base-layout";
    }
}
