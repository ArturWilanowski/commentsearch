package com.arturwilanowski.demoPost.repository;

import com.arturwilanowski.demoPost.dto.CommentDTO;
import com.arturwilanowski.demoPost.utils.RestTemplateUtil;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CommentsRepo extends RestTemplateUtil {

    protected static Logger LOG = Logger.getLogger(CommentDTO.class);
    private Response response;
    private String ENDPOINT_COMMENTS = "comments/";
    private String ENDPOINT_POSTS = "posts/";

    private static final String POST_ID = "postId";
    private static final String ID = "id";

    public List<CommentDTO> getResponseByPostId(String postId) {
        LOG.info("Executing GET request to Comments API with " + POST_ID + "=" + postId);
        response = executeGet(ENDPOINT_POSTS + postId + "/comments");

        return changeJsonToPostsArrayList(response);
    }

    public List<CommentDTO> getResponseById(String commentId) {
        Map<String, String> param = new HashMap<>();
        param.put(ID, commentId);
        LOG.info("Executing GET request to Comments API with " + ID + "=" + commentId);
        response = executeGet(ENDPOINT_COMMENTS, param);

        return changeJsonToPostsArrayList(response);
    }

    private List<CommentDTO> changeJsonToPostsArrayList(Response response) {
        return Arrays.asList(response.getBody().as(CommentDTO[].class));
    }
}
