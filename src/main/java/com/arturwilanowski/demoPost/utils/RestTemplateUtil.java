package com.arturwilanowski.demoPost.utils;


import com.arturwilanowski.demoPost.repository.CommentsRepo;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class RestTemplateUtil {

    private final String baseURI = "https://jsonplaceholder.typicode.com/";

    protected Response executeGet(String endpoint) {
        return given().contentType(ContentType.JSON).baseUri(baseURI).headers(createHeaders())
                .get(endpoint);
    }

    protected Response executeGet(String endpoint, Map<String, String> params) {
        return given().contentType(ContentType.JSON).baseUri(baseURI).headers(createHeaders()).params(params)
                .get(endpoint);
    }

    private Map<String, ?> createHeaders() {
        Map<String, Object> headers = new HashMap<>();
        return headers;
    }
}
