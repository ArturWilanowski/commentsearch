package com.arturwilanowski.demoPost.controller;

import com.arturwilanowski.demoPost.dto.CommentDTO;
import com.arturwilanowski.demoPost.repository.CommentsRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ControllerAppTest {

    @Test
    void indexResponseAllCommentsByFirstPostId() {
        String idPosta = "1";
        CommentDTO firstCommentDTO = firstCommentDTO();
        CommentDTO fiveCommentDTO = fiveCommentDTO();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseByPostId(idPosta);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertNotNull(commentDTOS.get(0));
        Assertions.assertNotNull(commentDTOS.get(4));
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());

        Assertions.assertEquals(fiveCommentDTO.getPostId(), commentDTOS.get(4).getPostId());
        Assertions.assertEquals(fiveCommentDTO.getId(), commentDTOS.get(4).getId());
        Assertions.assertEquals(fiveCommentDTO.getName(), commentDTOS.get(4).getName());
        Assertions.assertEquals(fiveCommentDTO.getEmail(), commentDTOS.get(4).getEmail());
        Assertions.assertEquals(fiveCommentDTO.getBody(), commentDTOS.get(4).getBody());
    }

    @Test
    void indexResponseAllCommentsByLastPostId() {
        String idPosta = "100";
        CommentDTO firstCommentDTO = firstCommentDTOforHundredPost();
        CommentDTO fiveCommentDTO = fiveCommentDTOforHundredPost();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseByPostId(idPosta);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertNotNull(commentDTOS.get(0));
        Assertions.assertNotNull(commentDTOS.get(4));
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());

        Assertions.assertEquals(fiveCommentDTO.getPostId(), commentDTOS.get(4).getPostId());
        Assertions.assertEquals(fiveCommentDTO.getId(), commentDTOS.get(4).getId());
        Assertions.assertEquals(fiveCommentDTO.getName(), commentDTOS.get(4).getName());
        Assertions.assertEquals(fiveCommentDTO.getEmail(), commentDTOS.get(4).getEmail());
        Assertions.assertEquals(fiveCommentDTO.getBody(), commentDTOS.get(4).getBody());
    }

    @Test
    void indexResponseAllCommentsByTwentyPostId() {
        String idPosta = "20";
        CommentDTO firstCommentDTO = firstCommentDTOforTwentyPost();
        CommentDTO fiveCommentDTO = fiveCommentDTOforTwentyPost();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseByPostId(idPosta);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertNotNull(commentDTOS.get(0));
        Assertions.assertNotNull(commentDTOS.get(4));
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());

        Assertions.assertEquals(fiveCommentDTO.getPostId(), commentDTOS.get(4).getPostId());
        Assertions.assertEquals(fiveCommentDTO.getId(), commentDTOS.get(4).getId());
        Assertions.assertEquals(fiveCommentDTO.getName(), commentDTOS.get(4).getName());
        Assertions.assertEquals(fiveCommentDTO.getEmail(), commentDTOS.get(4).getEmail());
        Assertions.assertEquals(fiveCommentDTO.getBody(), commentDTOS.get(4).getBody());
    }

    @Test
    void indexResponseFirstComment() {
        String commentId = "1";
        CommentDTO firstCommentDTO = firstCommentDTO();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseById(commentId);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());
    }

    @Test
    void indexResponseLastComment() {
        String commentId = "500";
        CommentDTO firstCommentDTO = fiveCommentDTOforHundredPost();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseById(commentId);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());
    }

    @Test
    void indexResponseoneHundredAndElevenChoiseComment() {
        String commentId = "111";
        CommentDTO firstCommentDTO = oneHundredAndElevenChoiseComment();

        CommentsRepo cm = new CommentsRepo();
        List<CommentDTO> commentDTOS = cm.getResponseById(commentId);

        Assertions.assertNotNull(commentDTOS);
        Assertions.assertEquals(firstCommentDTO.getPostId(), commentDTOS.get(0).getPostId());
        Assertions.assertEquals(firstCommentDTO.getId(), commentDTOS.get(0).getId());
        Assertions.assertEquals(firstCommentDTO.getName(), commentDTOS.get(0).getName());
        Assertions.assertEquals(firstCommentDTO.getEmail(), commentDTOS.get(0).getEmail());
        Assertions.assertEquals(firstCommentDTO.getBody(), commentDTOS.get(0).getBody());
    }

    private CommentDTO oneHundredAndElevenChoiseComment() {
        return new CommentDTO(
                "23",
                "111",
                "possimus facilis deleniti nemo atque voluptate",
                "Stefan.Crist@duane.ca",
                "ullam autem et\n" +
                        "accusantium quod sequi similique soluta explicabo ipsa\n" +
                        "eius ratione quisquam sed et excepturi occaecati pariatur\n" +
                        "molestiae ut reiciendis eum voluptatem sed");
    }

    private CommentDTO firstCommentDTO() {
        return new CommentDTO(
                "1",
                "1",
                "id labore ex et quam laborum",
                "Eliseo@gardner.biz",
                "laudantium enim quasi est quidem magnam voluptate ipsam eos\n" +
                        "tempora quo necessitatibus\n" +
                        "dolor quam autem quasi\n" +
                        "reiciendis et nam sapiente accusantium");
    }

    private CommentDTO fiveCommentDTO() {
        return new CommentDTO(
                "1",
                "5",
                "vero eaque aliquid doloribus et culpa",
                "Hayden@althea.biz",
                "harum non quasi et ratione\n" +
                        "tempore iure ex voluptates in ratione\n" +
                        "harum architecto fugit inventore cupiditate\n" +
                        "voluptates magni quo et");
    }

    private CommentDTO firstCommentDTOforHundredPost() {
        return new CommentDTO(
                "100",
                "496",
                "et occaecati asperiores quas voluptas ipsam nostrum",
                "Zola@lizzie.com",
                "neque unde voluptatem iure\n" +
                        "odio excepturi ipsam ad id\n" +
                        "ipsa sed expedita error quam\n" +
                        "voluptatem tempora necessitatibus suscipit culpa veniam porro iste vel");
    }

    private CommentDTO fiveCommentDTOforHundredPost() {
        return new CommentDTO(
                "100",
                "500",
                "ex eaque eum natus",
                "Emma@joanny.ca",
                "perspiciatis quis doloremque\n" +
                        "veniam nisi eos velit sed\n" +
                        "id totam inventore voluptatem laborum et eveniet\n" +
                        "aut aut aut maxime quia temporibus ut omnis");
    }

    private CommentDTO firstCommentDTOforTwentyPost() {
        return new CommentDTO(
                "20",
                "96",
                "non sit ad culpa quis",
                "River.Grady@lavada.biz",
                "eum itaque quam\n" +
                        "laboriosam sequi ullam quos nobis\n" +
                        "omnis dignissimos nam dolores\n" +
                        "facere id suscipit aliquid culpa rerum quis");
    }

    private CommentDTO fiveCommentDTOforTwentyPost() {
        return new CommentDTO(
                "20",
                "100",
                "et sint quia dolor et est ea nulla cum",
                "Leone_Fay@orrin.com",
                "architecto dolorem ab explicabo et provident et\n" +
                        "et eos illo omnis mollitia ex aliquam\n" +
                        "atque ut ipsum nulla nihil\n" +
                        "quis voluptas aut debitis facilis");
    }
}